package com.gurdil.pokolan.gateway.client.keycloak;

import com.gurdil.pokolan.gateway.client.keycloak.dto.KeycloakValidationResponse;
import com.gurdil.pokolan.gateway.configuration.properties.KeycloakConfigurationProperties;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@Log4j2
public class KeycloakClient {

    private final KeycloakConfigurationProperties kcProperties;
    private final WebClient client;

    public KeycloakClient(final KeycloakConfigurationProperties keycloakConfigurationProperties) {
        this.kcProperties = keycloakConfigurationProperties;
        this.client = WebClient.builder()
                               .baseUrl(keycloakConfigurationProperties.getAuth_server_url())
                               .filter(logRequest())
                               .build();
    }

    public Mono<KeycloakValidationResponse> validateBearerToken(String bearerToken) {
        //todo: improve error management
        return prepareAndRunRequest(bearerToken).retrieve()
                                                .bodyToMono(KeycloakValidationResponse.class)
                                                .flatMap(Mono::just);
                                                /*.onErrorResume(WebClientResponseException.class,
                                                               ex -> Mono.just(false));*/
    }

    private WebClient.RequestHeadersSpec<?> prepareAndRunRequest(String bearerToken) {

        return client.post()
                     .uri(uriBuilder -> uriBuilder.path("/auth/realms/")
                                                  .path(kcProperties.getRealm())
                                                  .path("/protocol/openid-connect/token/introspect")
                                                  .build())
                     .contentType(MediaType.APPLICATION_JSON)
                     .body(BodyInserters.fromFormData("client_id", kcProperties.getResource())
                                        .with("client_secret", kcProperties.getCredentials_secret())
                                        .with("token", bearerToken));
    }

    // This method returns filter function which will log request data
    private static ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            log.debug("Request: {} {}", clientRequest.method(), clientRequest.url());
            clientRequest.headers()
                         .forEach((name, values) -> values.forEach(value -> log.info("{}={}", name, value)));
            log.debug("Attributes ={}", clientRequest.attributes());
            return Mono.just(clientRequest);
        });
    }

}
