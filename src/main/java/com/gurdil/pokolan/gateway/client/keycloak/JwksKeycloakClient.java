package com.gurdil.pokolan.gateway.client.keycloak;

import java.net.MalformedURLException;
import java.net.URL;

import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.JwkProviderBuilder;
import com.gurdil.pokolan.gateway.configuration.properties.KeycloakConfigurationProperties;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class JwksKeycloakClient {

    private final JwkProvider provider;
    private final KeycloakConfigurationProperties keycloakProperties;


    public JwksKeycloakClient(KeycloakConfigurationProperties keycloakProperties) throws MalformedURLException {
        this.keycloakProperties = keycloakProperties;

        String url = keycloakProperties.getAuth_server_url() + "auth/realms/" + keycloakProperties.getRealm() + "/protocol/openid-connect/certs";
        this.provider = new JwkProviderBuilder(new URL(url)).cached(true)
                                                            .rateLimited(false)
                                                            .build();
    }
}
