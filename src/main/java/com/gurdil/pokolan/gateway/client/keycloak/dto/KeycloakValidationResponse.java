package com.gurdil.pokolan.gateway.client.keycloak.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeycloakValidationResponse {

    private Boolean active;
    private String sub;

    @JsonProperty("realm_access")
    private RealmAccess realmAccess;

    @Setter
    @Getter
    public static class RealmAccess {
        private List<String> roles;
    }

}
