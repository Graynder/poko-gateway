package com.gurdil.pokolan.gateway.configuration.properties;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties
@Configuration
@ConfigurationProperties(prefix = "keycloak")
@Getter
@Setter
public class KeycloakConfigurationProperties {

    private boolean online;

    private String realm;

    private String auth_server_url;

    private String resource;

    private String credentials_secret;

}
