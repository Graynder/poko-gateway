package com.gurdil.pokolan.gateway.common.exception.jwks;

public class JwksProviderException extends RuntimeException {

    public JwksProviderException(String message) {
        super(message);
    }
}
