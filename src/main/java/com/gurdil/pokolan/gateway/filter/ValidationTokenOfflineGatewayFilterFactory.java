package com.gurdil.pokolan.gateway.filter;

import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.gurdil.pokolan.gateway.client.keycloak.JwksKeycloakClient;
import com.gurdil.pokolan.gateway.client.keycloak.dto.KeycloakValidationResponse;
import com.gurdil.pokolan.gateway.common.exception.jwks.JwksProviderException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
@Log4j2
public class ValidationTokenOfflineGatewayFilterFactory extends AbstractGatewayFilterFactory<ValidationTokenOfflineGatewayFilterFactory.Config> {

    private final JwksKeycloakClient jwkStore;

    public ValidationTokenOfflineGatewayFilterFactory(JwksKeycloakClient jwkStore) {
        super(Config.class);
        this.jwkStore = jwkStore;
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {

            String token = exchange.getRequest()
                                   .getHeaders()
                                   .get(AUTHORIZATION)
                                   .get(0);

            token = token.substring(7);
            log.trace("-- ValidationToken(): token={}", token);

            KeycloakValidationResponse response = validationToken(token);

            if (response.getActive()) {
                log.debug("-- ValidationToken(): Token valid");
                exchange.getAttributes()
                        .put("tokenValidated", response);
                return chain.filter(exchange);
            } else {
                log.warn("-- ValidationToken(): Token invalid");
                exchange.getResponse()
                        .setStatusCode(HttpStatus.UNAUTHORIZED);

                return exchange.getResponse()
                               .setComplete();
            }
        };
    }

    private KeycloakValidationResponse validationToken(String token) {

        KeycloakValidationResponse response = new KeycloakValidationResponse();

        DecodedJWT jwtDecoded = JWT.decode(token);
        Algorithm algorithm = generateAlgorithmFromKid(jwtDecoded.getKeyId());
        JWTVerifier verifier = buildJwtVerifier(algorithm);

        try {
            verifier.verify(token);
        } catch (JWTVerificationException exception) {
            response.setActive(false);
        }

        response.setActive(true);
        response.setSub(jwtDecoded.getSubject());
        response.setRealmAccess(jwtDecoded.getClaims()
                                          .get("realm_access")
                                          .as(KeycloakValidationResponse.RealmAccess.class));

        return response;
    }

    private JWTVerifier buildJwtVerifier(Algorithm algorithm) {
        return JWT.require(algorithm)
                  .build();
    }

    private Algorithm generateAlgorithmFromKid(String kid) {

        Jwk jwk;
        RSAPublicKey rsaPublicKey = null;
        ECPublicKey ecPublicKey = null;

        try {
            jwk = jwkStore.getProvider()
                          .get(kid);

            if (jwk.getType()
                   .equals("RSA")) {
                rsaPublicKey = (RSAPublicKey) jwk.getPublicKey();
            } else {
                ecPublicKey = (ECPublicKey) jwk.getPublicKey();
            }

        } catch (JwkException e) {
            log.error(e.getMessage());
            throw new JwksProviderException(e.getMessage());
        }

        log.debug(jwk.getAlgorithm());
        switch (jwk.getAlgorithm()) {
            case "RS256":
                return Algorithm.RSA256(rsaPublicKey, null);
            case "RS384":
                return Algorithm.RSA384(rsaPublicKey, null);
            case "RS512":
                return Algorithm.RSA512(rsaPublicKey, null);
            case "ES256":
                return Algorithm.ECDSA256(ecPublicKey, null);
            case "ES384":
                return Algorithm.ECDSA384(ecPublicKey, null);
            case "ES512":
                return Algorithm.ECDSA512(ecPublicKey, null);
            default:
                log.error("Algorithm not available");
                throw new RuntimeException("Algorithm not available");
        }
    }

    @Getter
    @Setter
    public static class Config {
    }
}
