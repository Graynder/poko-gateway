package com.gurdil.pokolan.gateway.filter;

import com.gurdil.pokolan.gateway.client.keycloak.KeycloakClient;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
@Log4j2
public class ValidationTokenOnlineGatewayFilterFactory extends AbstractGatewayFilterFactory<ValidationTokenOnlineGatewayFilterFactory.Config> {

    private final KeycloakClient client;

    public ValidationTokenOnlineGatewayFilterFactory(KeycloakClient client) {
        super(Config.class);
        this.client = client;
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {

            String token = exchange.getRequest()
                                   .getHeaders()
                                   .get(AUTHORIZATION)
                                   .get(0);

            token = token.substring(7);
            log.trace("-- ValidationToken(): token={}", token);

            return client.validateBearerToken(token)
                         .flatMap(response -> {
                             if (response.getActive()) {
                                 log.debug("-- ValidationToken(): Token valid");
                                 exchange.getAttributes()
                                         .put("tokenValidated", response);
                                 return chain.filter(exchange);
                             } else {
                                 log.warn("-- ValidationToken(): Token invalid");
                                 exchange.getResponse()
                                         .setStatusCode(HttpStatus.UNAUTHORIZED);

                                 return exchange.getResponse()
                                                .setComplete();
                             }
                         });
        };
    }


    @Getter
    @Setter
    public static class Config {
    }
}
