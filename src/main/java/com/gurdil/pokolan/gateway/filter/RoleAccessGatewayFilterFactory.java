package com.gurdil.pokolan.gateway.filter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.gurdil.pokolan.gateway.client.keycloak.dto.KeycloakValidationResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class RoleAccessGatewayFilterFactory extends AbstractGatewayFilterFactory<RoleAccessGatewayFilterFactory.Config> {

    public RoleAccessGatewayFilterFactory() {
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            String[] roles = config.authorizedRoles.split("\\|");
            KeycloakValidationResponse response = exchange.getAttribute("tokenValidated");

            boolean status = Arrays.stream(roles)
                                   .anyMatch(role -> response.getRealmAccess()
                                                             .getRoles()
                                                             .contains(role));

            if (status) {
                return chain.filter(exchange);
            } else {
                exchange.getResponse()
                        .setStatusCode(HttpStatus.UNAUTHORIZED);

                return exchange.getResponse()
                               .setComplete();
            }
        };
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Collections.singletonList("authorizedRoles");
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Config {
        private String authorizedRoles;
    }

}
